package com.myzee;

import java.util.HashMap;

/*
 * Hashmap is stronger than GC.
 * if any object is associated with hashmap and dont have reference, then hashmap will not
 * allow GC to collect it. But where as in weakhashmap it will allow to collect it.
 */

public class GCWontCollectKeyObject {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Temp t = new Temp();
		HashMap<Temp, String> hm = new  HashMap<>();
		hm.put(t, "temp");
		System.out.println(hm);
		t = null;	// should be eligible for garbage collection, 
					// but it wont be because it still associated with hashmap.
					// so, hashmap wont allow gc to collect it
		System.gc();	//calling gc.
		System.out.println(hm);
		
		// create Temp object, make it null.
		// call System.gc()
		// finalize() method will be called
		Temp t1 = new Temp();
		t1 = null;
		System.gc();
		
	}

}

class Temp {
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "temp object";
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#finalize()
	 * This method will be called whenever GC starts collecting Temp object.
	 */
	@Override
	protected void finalize() throws Throwable {
		// TODO Auto-generated method stub
		System.out.println("calling finalize method on temp object");
		super.finalize();
	}
}
